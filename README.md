Koumbit's git hooks collection
==============================

Those hooks are either original code or modification from upstream
versions that were having issues. Hopefully the upstream versions can
eventually be merged upstream.

archive-tag
-----------

This script will create a tarball (using git archive) out of tags
pushed to the repository, optionally (if modules are available)
outputting the checksum to the user. The script is controlled through
git configuration, see the script's usage for more information.

This is to be run as a post-receive hook.

post-receive-checkout-copy
--------------------------

This hook keeps a checkout of the current bare repository up to
date. It assumes the checkout is not manually modified: it performs
only a fast-forward on the repository.

post-receive-checkout-force
---------------------------

This dumb hook forces a checkout after a "push" to a git repository if
the repository is non-bare. It is really a hack to allow a non-bare
repository to be pushed to.

In addition to this script the `receive.denyCurrentBranch`
configuration option needs to be set to `warn` or `ignore` for those
pushes to work.

post-receive-irker
------------------

A wrapper around `irker-hook.py` which will call it with the right
arguments to be ran from a `post-receive` hook.

post-update-weblate
-------------------

A sample hook to ping a website on update. Should be made more
generic.

update-verify-puppet-syntax
---------------------------

This hook, if linked to "update" hook, will run a syntax check on all puppet
manifests and all erb templates. If any syntax error is present, it will refuse
the update.

markdown-readme
---------------

This hook will render a `README.mdwn` file (like this one) into HTML
for `gitweb`. `gitweb` will render the resulting HTML file is its
`$prevent_xss` configuration option is set to `false`.

This script takes no argument but expects `$GIT_DIR` to be set, so it
can be ran in `post-commit` or `post-receive`.
