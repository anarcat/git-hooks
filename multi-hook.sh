#!/bin/bash

# @SOURCE: https://stackoverflow.com/a/33995812
# Allow to run multiple hooks
# For each hook type (post-receive, post-update, ...) create a type.d subdirectory, e.g. post-receive.d/
# Then put all hook scripts into that directory and make them executable.

script_dir=$(dirname $0)
hook_name=$(basename $0)

tmpfile=$(mktemp "${hook_name}"XXXX)
trap "rm -f '$tmpfile'" EXIT
cat > "$tmpfile"

hook_dir="$script_dir/$hook_name.d"
if [[ -d "$hook_dir" ]]; then
    for i in "$hook_dir"/* ; do
       "$i" "$@" < "$tmpfile"
       if [[ $? -ne 0 ]] ; then
         exit -1
       fi
    done
fi

exit 0
